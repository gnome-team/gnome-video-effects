gnome-video-effects (0.6.0-3) unstable; urgency=medium

  * Opt into Salsa CI

 -- Jeremy Bícha <jbicha@ubuntu.com>  Sat, 18 Jan 2025 12:43:06 -0500

gnome-video-effects (0.6.0-2) unstable; urgency=medium

  [ Jarred Wilson ]
  * Add debian/upstream/metadata

  [ Jeremy Bícha ]
  * Bump debhelper compat to 13
  * Bump Standards Version to 4.7.0
  * Add Breaks/Replaces for gnome-video-effects-extra
  * Opt into dpkg v1 build API
  * Stop using debian/control.in and gnome-pkg-tools

 -- Jeremy Bícha <jbicha@ubuntu.com>  Fri, 17 Jan 2025 16:14:42 -0500

gnome-video-effects (0.6.0-1) unstable; urgency=medium

  * New upstream release
    - Fix flip effect with gstreamer 1.22 (Closes: #1037514, LP: #2023675)
  * gnome-video-effects-frei0r.install: Install new scanlines effect

 -- Jeremy Bícha <jbicha@ubuntu.com>  Tue, 13 Jun 2023 13:49:56 -0400

gnome-video-effects (0.5.0-1) unstable; urgency=medium

  * New upstream version
  * debian/control.in:
    - build-depends on meson

 -- Sebastien Bacher <seb128@ubuntu.com>  Tue, 13 Aug 2019 11:07:01 +0200

gnome-video-effects (0.4.3-3) unstable; urgency=medium

  * Disable parallel building since it can break translations.
    Detected by Debian Reproducible Builds.
  * Bump Standards-Version to 4.3.0
  * Build-Depend on debhelper-compat 12 and drop debian/compat

 -- Jeremy Bicha <jbicha@debian.org>  Wed, 26 Dec 2018 07:05:12 -0500

gnome-video-effects (0.4.3-2) unstable; urgency=medium

  * Update Vcs fields for migration to https://salsa.debian.org/
  * Add debian/gbp.conf
  * Bump minimum debhelper compat to 11
  * Bump Standards-Version to 4.1.3

 -- Jeremy Bicha <jbicha@debian.org>  Tue, 23 Jan 2018 22:11:16 -0500

gnome-video-effects (0.4.3-1) unstable; urgency=medium

  * New upstream release.
  * debian/gnome-video-effects-frei0r.install:
    - follow upstreams "inversion effect" spelling fix
  * Bump dh compat to 10 (from deprecated 7)
  * Bump Standards-Version to 3.9.8
  * Switch from CDBS to dh
  * Add debian/docs to explicitly ship AUTHORS, NEWS, README
    - previously magically done by CDBS (in every binary package)
  * debian/rules: Use --fail-missing instead of --list-missing
  * Drop Luciana Fujii Pontello (Personal mail) from uploaders
    - No activity since 2011, thanks for your past work!
  * Bump Standards-Version to 4.0.1
  * Switch from dh_install --fail-missing to dh_missing --fail-missing.
  * Add gnome-get-source.mk include to debian/rules

 -- Andreas Henriksson <andreas@fatal.se>  Thu, 17 Aug 2017 21:19:55 +0200

gnome-video-effects (0.4.1-3) unstable; urgency=low

  [ Jean Schurger ]
  * debian/control.in:
    - Fixed Homepage URL
    - Bump Standards-Version to 3.9.6 (no further changes)
    - Rephrased the description to relax lintian 'description-is-pkg-name'

 -- Andreas Henriksson <andreas@fatal.se>  Thu, 24 Sep 2015 15:44:45 +0200

gnome-video-effects (0.4.1-2) unstable; urgency=medium

  * debian/control.in:
    + Update dependencies for GStreamer 1.0. The package was already ported
      but the dependencies were not updated. Closes: #753009.

 -- Emilio Pozuelo Monfort <pochu@debian.org>  Sat, 28 Jun 2014 17:30:28 +0200

gnome-video-effects (0.4.1-1) unstable; urgency=low

  [ Jeremy Bicha ]
  * Don't have gnome-video-effects-dev depend on gnome-video-effects-frei0r
  * Update homepage

  [ Andreas Henriksson ]
  * New upstream release.

 -- Andreas Henriksson <andreas@fatal.se>  Fri, 09 May 2014 19:58:31 +0200

gnome-video-effects (0.4.0-1) unstable; urgency=low

  * New upstream release
  * debian/gnome-video-effects.install: Install new flip effect
  * debian/control.in: Bump Standards-Version to 3.9.3 (no further changes)
  * debian/watch: Track .xz tarball

 -- Laurent Bigonville <bigon@debian.org>  Tue, 03 Apr 2012 21:27:23 +0200

gnome-video-effects (0.3.0-4) unstable; urgency=low

  * debian/control: Split effects that require frei0r-plugins in their own
    package
  * debian/rules: List missing files, if any

 -- Laurent Bigonville <bigon@debian.org>  Tue, 09 Aug 2011 09:26:23 +0200

gnome-video-effects (0.3.0-3) unstable; urgency=low

  * debian/control.in:
    - Make gnome-video-effects depends against frei0r-plugins

 -- Laurent Bigonville <bigon@debian.org>  Fri, 08 Jul 2011 16:34:04 +0200

gnome-video-effects (0.3.0-2) unstable; urgency=low

  * debian/control.in:
    - Bump Standards-Version to 3.9.2 (no further changes)
    - Drop duplicate Section
    - Add Vcs- fields
    - Add uploaders.mk magic to Uploaders field
    - Make gnome-video-effects depends against gstreamer0.10-plugins-good and
      gstreamer0.10-plugins-bad
  * debian/watch: Add watch file

 -- Laurent Bigonville <bigon@debian.org>  Fri, 08 Jul 2011 14:12:30 +0200

gnome-video-effects (0.3.0-1) experimental; urgency=low

  * Initial Release (Closes: #621806)

 -- Luciana Fujii Pontello (Personal mail) <luciana@fujii.eti.br>  Sat, 16 Apr 2011 17:36:45 -0300
